using UnityEngine;
using System.Collections;

public class PixelizeScreen : MonoBehaviour {	
	public int pixelize = 1;
	protected void Start() {
		if (!SystemInfo.supportsImageEffects) {
			enabled = false;
			return;
		}
	}
	void OnRenderImage (RenderTexture source, RenderTexture destination) {		
		RenderTexture buffer = RenderTexture.GetTemporary(source.width/pixelize, source.height/pixelize, 0);
		buffer.filterMode = FilterMode.Point;
		Graphics.Blit(source, buffer);	
		Graphics.Blit(buffer, destination);
		RenderTexture.ReleaseTemporary(buffer);
	}
}